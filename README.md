# Beyond Equilibrium: Electronic Structure Methods for Mesoscale Systems

Talk at the "Non-equilibrium Phenomena, Nonadiabatic Dynamics and Spectroscopy" conference in Telluride, CO (10/1-10/6/2023).

## To fully enjoy it

Make sure to `pip install dftpy qepy`


## Contact

Michele Pavanello
m.pavanello@rutgers.edu
@MikPavanello